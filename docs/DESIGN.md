## Architecture of the App

### Tech Stack
- NodeJS
- ReactJS
- Spectre.css
- Fastify
- Socket.io
- Firebase
- PostgreSQL
- GithubAPI GraphQL-Client


### Flow
client entry form
  -> server create a room
  -> server fetch data
  -> server send back the data
