const
  fp = require('fastify-plugin')
  , socketIO  = require('socket.io')

module.exports = fp(function(fy, opts, next) {
  fy.decorate('io', socketIO(fy.server))
  next()
})
