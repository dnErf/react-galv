const
  fp = require('fastify-plugin')

module.exports = fp(function(fy, opts, next) {
  let { io, redis, dbTicket } = fy

  io.on('connection', (socket) => {

    console.log('client on', socket.id)
    let groups = Groups()

    socket.on('createTicket', (params, callback) => {

      if (!groups.getGroup(socket.id)) {
        groups.addGroup(socket.id, params)
      }

      // let newTicket = new dbTicket(params)
      //
      // newTicket
      //   .save()
      redis
        .set(params.repository.id, JSON.stringify(params), 'EX', 60);
      // socket
      //   .join(params.repository.id)
      io
        .emit('newTicket', params)

      // console.log('grouped ticket', groups.getGroupList())

    })

    socket.on('joinTicket', (params, callback) => {
      // TODO
      socket
        .join(params.repository.id)
      callback(params.repository.id)
    })

    socket.on('listMounted', (callback) => {
      dbTicket
        .find({})
        .then((records) => {
          if (records) {
            callback(records)
          }
          return
        })
        .catch((err) => {
          console.log(err)
        })
    })

    socket.on('broadcastCode', (params, callback) => {
      let group = groups.getGroup(socket.id)
      if (group) {
        socket.to(group.ticket.repository.id).emit('codeStream', params)
        callback()
      }
    })

  })

  next()

})

function Groups() {
  let groups = []
  return {
    addGroup (id, ticket) {
      groups.push({id,ticket})
    },
    getGroup (id) {
      return groups.filter((g) => g.id === id)[0]
    },
    getGroupList () {
      return groups
    },
    delGroup (id) {
      if (this.getGroup(id)) {
        groups = groups.filter((g) => g.id !== id)
        return true
      }
      return false
    }
  }
}
