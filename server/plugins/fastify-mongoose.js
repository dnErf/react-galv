const
  fp = require('fastify-plugin')
  , mongoose  = require('mongoose')

  module.exports = fp(function(fy, opts, next) {

    mongoose
      .connect('mongodb://192.168.99.100:27017/hrr', { useNewUrlParser: true})
      .then(() => console.log('mongodb connected'))
      .catch(err => console.log(err))
    let
      { Schema, model } = mongoose
    const
      TicketSchema = new Schema({
        git: String,
        desc: String,
        repository: {
          id: String,
          name: String,
          object: {
            oid: String,
            text: String,
            isBinary: Boolean
          }
        }
      }, { timestamps: true })
    TicketSchema.index({ createdAt: 1 }, { expireAfterSeconds: 7200 });
    fy.decorate('dbTicket', model('ticket', TicketSchema))
    next()

  })

  /*
  context = [
    {
      git: '',
      desc: '',
      repository: {
        id: '',
        name: '',
        object: {
          oid: '',
          text: '',
          isBinary: false
        }
      }
    }
  ]
  */
