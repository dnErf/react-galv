const
  path      = require('path')
  , fy      = require('./fy.js')
  , port    = process.env.PORT || 9999

module.exports = function() {
  fy.listen(port, () => console.log('galv listens on ' + port + ':' + process.pid))
}
