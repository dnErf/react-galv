const
  fy = require('fastify')()
  , to = require('./api/routes.js')

fy
  .register(require('fastify-cors'))
  .register(require('fastify-compress'))
  .register(require('fastify-redis'), { host: '192.168.99.100' })
  .register(require('./plugins/fastify-mongoose'))
  .register(require('./plugins/fastify-socket.io'))
  .register(require('./plugins/fy-process'))
  .get('/', (req, res) => {
    res.send({ hello: 'world' })
  })
  .route(to.postTicket)

module.exports = fy
