const
  axios = require('axios')
  , ghGQL = axios.create({
    baseURL: 'https://api.github.com/graphql',
    headers: {
      Authorization: 'bearer ' + process.env.GITHB_BEARER
    }
  })
  , query = `query ($owner:String!,$repo:String!,$expr:String!) {
    repository(owner: $owner, name: $repo) {
      id
      name
      object (expression: $expr) {
        oid
        ... on Blob {
          text
          isBinary
        }
      }
    }
  }`

module.exports = function (owner, repo, expr) {
  return ghGQL
    .post('', {
      query ,
      variables: { owner, repo, expr }
    })
}
