const gh = require('./github.js')

let postTicket = {
  url: '/api/ticket',
  method: 'POST',
  handler: (req,res) => {

    // let
    //   urlStr = req.body.git
    //   , urlArr = urlStr.split(/\//g)
    //   , exprStr = ''
    //
    // for (let i = urlArr.length - 1; i > 0; i--) {
    //   if (urlArr[i - 1] === 'blob') {
    //     exprStr = urlArr[i] + ':' + exprStr
    //     break;
    //   }
    //   else {
    //     exprStr = urlArr[i] + (i !== urlArr.length - 1 ? '/' : '') + exprStr
    //   }
    // }
    //
    // gh(urlArr[3], urlArr[4], exprStr)
    //   .then((result) => {
    //     res.send(result.data.data)
    //   })
    //   .catch((err) => {
    //     console.log('failed')
    //   })

    res.send({
      "repository": {
        "id": "MDEwOlJlcG9zaXRvcnkxNjI4MzIwNTU=",
        "name": "url-stack",
        "object": {
          "oid": "f32b30045ed183ea3db1c517c09b225f3bd71dff",
          // "text": "import './style.js'\n\nimport m from 'mithril'\nimport b from 'bss'\n\nimport ac from './data/action-control'\n\nimport Menu from './components/menu.js'\nimport Content from './components/content.js'\n\nwindow.m = m\nb.setDebug(true)\n\nhljs.initHighlightingOnLoad();\n\n// const app = {\n//   view (v) {\n//     return m('.app.fbx', [\n//       m(Menu) ,\n//       m(Content,ac)\n//     ])\n//   }\n// }\n// m.mount(document.body, app)\n\nm.route(document.body, '/hrext', {\n  '/:data' : {\n    onmatch (args) {\n      if (ac.data === null || ac.data === undefined) {\n        ac.fetchData()\n      }\n      else {\n        ac.switchData(args.data)\n      }\n      // ac.selectedData = args.data\n      // ac.model = ac.data[ac.selectedData]\n      // ac.reset()\n    } ,\n    render (v) {\n      return m('.app.fbx', [\n        m(Menu,\n          {\n            categories:Object.keys(ac.data) ,\n            newCategory : ac.newCategory ,\n            deleteCategory : ac.deleteCategory ,\n          }\n        ) ,\n        m(Content,ac)\n      ])\n    }\n  }\n})\n\n\n",
          "isBinary": false
        }
      }
    })

  }
}

module.exports = {
  postTicket
}
