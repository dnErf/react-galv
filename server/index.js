const
  cluster = require('cluster')
  , cpus  = require('os').cpus().length
  , start = require('./server.js')

switch(cluster.isMaster) {
  case true : distribute(); break;
  case false :
  default : start();
}

function distribute() {
  let
    workers = []
    , createClust = (i) => {
      workers[i] = cluster.fork()
      workers[i].on('exit', () => createClust(i))
    }
  for (let core = 0; core < 1; core++) {
    createClust(core)
  }
}
