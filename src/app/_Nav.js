import React from 'react'

function Nav() {
  return (
    <header className="navbar bb">
      <section className="navbar-section">
        <a href="/" className="navbar-brand mr-2">GalV</a>
      </section>
      <section className="navbar-section">
        <a href="/" className="btn btn-link">Login with Github</a>
      </section>
    </header>
  )
}

export default Nav
