import React, { useState, useEffect } from 'react'

import CodeMirror from 'codemirror'
import 'codemirror/theme/mdn-like.css'
import 'codemirror/mode/javascript/javascript.js'

function Resolute(props) {
  let
    cm = null
    , ta = null
    , { io, repository } = props
    , [codeTxt, setCodeTxt] = useState('')
  useEffect(function onMount() {
    cm = CodeMirror.fromTextArea(ta, {
      theme: 'mdn-like',
      mode: 'javascript',
      styleActiveLine: true,
      lineNumbers: true
    })
    cm.on('keyup', () => {
      io.emit('broadcastCode', cm.getValue(), () => {
        console.log('code broadcasted')
      })
    })
    io.on('codeStream', (data) => {
      cm.getDoc().setValue(data)
      // cm.setCursor(cm.getCursor())
    })
    if (repository.object.text) {
      setCodeTxt(repository.object.text)
      cm.getDoc().setValue(repository.object.text)
    }
  }, [])
  // useEffect(function onCmValueChange() {
  //   cm.getDoc().setValue(repository.object.text)
  // }, [repository.object.text])
  console.log(props)
  return (
    <div className="container mt-2">
      Resolute
      <div className="columns">
        <div className="column col-2">
          <figure className="avatar avatar-xl p-centered">
            <img alt="..." />
          </figure>
          <ul className="nav">
            <li className="nav-item active">
              <a href="/resolute">Help</a>
            </li>
            <li className="nav-item">
              <a href="/resolute">Oops</a>
            </li>
          </ul>
        </div>
        <div className="column col-10">
          <div className="panel">
            <div className="panel-header">
              <div className="panel-title h5">{repository.name}</div>
            </div>
            <div className="panel-body">
              <div className="form-group px-2">
                <textarea ref={node => ta = node } className="form-input help--desc" id="help-desc" rows="24"></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Resolute
