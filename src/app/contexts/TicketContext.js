import React, { useState, useReducer } from 'react'

export const Ctx = React.createContext({})

export default (props) => {
  const
    initialState = {
      git: '',
      desc: '',
      repository : {
        id: '',
        object: {
          oid: '',
          text: '',
          isBinary: false
        }
      }
    }
    , [data, dispatch] = useReducer(taper, [])
    , [state, setState] = useState(initialState)
  return (
    <Ctx.Provider value={{ state, setState, data, dispatch}}>
      {props.children}
    </Ctx.Provider>
  )
}

function taper(store, action) {
  switch(action.type) {
    case 'ADD':
      store.push(action.payload)
      return store
    case 'DEL':
      return store.filter((ticket) => action.payload !== ticket.git)
    case 'RESET':
      return []
    default: return store
  }
}

/*
context = [
  {
    git: '',
    desc: '',
    repository: {
      id: '',
      name: '',
      object: {
        oid: '',
        text: '',
        isBinary: false
      }
    }
  }
]
*/
