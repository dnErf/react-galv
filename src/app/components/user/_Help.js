import React, { createRef } from 'react'
import { withRouter } from 'react-router-dom'
import axios from 'axios'

// import { Ctx } from '../../contexts/TicketContext'

function Help(props) {
  const
    refGit = createRef()
    , refDesc = createRef()
  let
    handleSubmit = (e) => {
      e.preventDefault()
      let
        git = refGit.current.value
        , desc = refDesc.current.value
      axios
        .post('http://localhost:9999/api/ticket', {
          git, desc
        })
        .then((result) => {
          if (result.status !== 200) {
            throw Error('failed')
          }
          let newTicket = {
            git, desc, ...result.data
          }
          props.io.emit('createTicket', newTicket ,function() {
            console.log('ticket created')
          })
          props.history.push(`/resolute/${newTicket.repository.id}`, {...newTicket})
          // props.history.push('/resolute/MDQ6QmxvYjE2MjgzMjA1NTpm', {...newTicket})
        })
        .catch((err) => {
          console.log('failed to fetch data')
        })
    }
  return (
    <div className="hero hero-sm bg-gray">
      <div className="p-centered w-50">
        <div className="form-group ">
          <label className="form-label" htmlFor="git-url">Git URL of the source file...</label>
          <input className="form-input" type="text" id="git-url" placeholder="Git URL" ref={refGit} />
          <textarea className="form-input help--desc" id="help-desc" placeholder="Short Description of the Issue" rows="3" ref={refDesc}></textarea>
        </div>
        <div className="form-group float-right">
          <button className="btn btn-primary" onClick={handleSubmit}>Submit</button>
        </div>
      </div>
    </div>
  )
}

export default withRouter(Help)


// setting context state at this level is not working
