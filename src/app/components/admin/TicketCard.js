import React from 'react'
import { Link } from 'react-router-dom'

export default ({name, desc, ticket}) => (
  <div className="card">
    <div className="card-header">
      <div className="card-title h5"><Link to={{pathname: `/resolute/${ticket.repository.id}`, state: ticket}}>{name}</Link></div>
      <div className="card-subtitle text-gray">{desc}</div>
    </div>
  </div>
)
