import React, { useContext, useEffect, useState } from 'react'

import TicketCard from './TicketCard'
import { Ctx } from '../../contexts/TicketContext'

function TicketList(props) {
  const
    ctx = useContext(Ctx)
    , [tickets, setTickets] = useState([])
  useEffect(function onMount() {
    function render() {
      setTickets(
        ctx.data.map((item, i) => (
          <div key={i} className="column col-3 col-xs-12 mt-2">
            <TicketCard key={i} name={item.repository.name} desc={item.desc} ticket={item} />
          </div>
        ))
      )
    }
    props.io.emit('listMounted', (payload) => {
      if (payload.length - 1 !== ctx.data.length - 1) {
        payload.forEach(({ git, desc, repository })=>{
            let
              newTicket = {
                git, desc, repository
              }
          ctx.dispatch({type: 'ADD', payload: newTicket})
        })
      }
      render()
    })
    props.io.on('newTicket', (payload) => {
      ctx.dispatch({type:'ADD', payload})
      render()
    })
  }, [])
  // item.repository.id
  return (
    <div className="columns">
      {
        tickets.length > 0
        ? tickets
        : ''
      }
    </div>
  )
}

export default TicketList

/*
context = [
  {
    git: '',
    desc: '',
    repository: {
      id: '',
      name: '',
      object: {
        oid: '',
        text: '',
        isBinary: false
      }
    }
  }
]
*/
