import React, { useEffect } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import TicketContext from './contexts/TicketContext'
import useSocketIO from './hooks/useSocketIO'

import Nav from './_Nav'
import Resolute from './_Resolute'

import TicketList from './components/admin/_TicketList'
import Help from './components/user/_Help'

function App() {
  let
    [io] = useSocketIO('http://localhost:9999')
  useEffect(function onMount() {
    io.on('connect', function() {
      console.log('connected to socket.io')
    })
  }, [])
  return (
    <div className="container" style={{ height: '100vh', minHeight: '100%' }}>
      <Nav />
      <Router>
        <TicketContext>
          <Route exact path="/" component={(props) => <Help io={io}/>} />
          <Route
          path="/resolute/:group"
          component={(props) => {
            io.emit('joinTicket', props.location.state, (data)=>{
              console.log('join ticket ' + data)
            })
            return <Resolute io={io} {...props.location.state}/>
          }}
          />
          <Route exact path="/tickets" component={() => <TicketList io={io}/>} />
        </TicketContext>
      </Router>
    </div>
  )
}

export default App
