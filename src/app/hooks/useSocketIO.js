import React, { useState } from 'react'
import socketIO from 'socket.io-client'

export default (url) => {
  const
    [ ioURL, setURL ] = useState(url)
  let
    io = socketIO(ioURL)
  return [io, setURL]
}
